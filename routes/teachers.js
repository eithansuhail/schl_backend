const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('config');
const auth = require('../middleware/authteacher');

const { check, validationResult } = require('express-validator');

const Teacher = require('../models/Teacher');

// @route    DELETE /:teacher_id
// @desc     Delete a post
// @access   Public
router.delete('/:id', async (req, res) => {
    try {
        // Get sudent id for delete it.
        const teacher = await Teacher.findOne({ "teacher_id": req.params.id });

        if (!teacher) {
            return res.status(404).json({ msg: 'teacher not found' });
        }
        await teacher.remove();

        res.json({ msg: 'teacher removed' });
    } catch (err) {
        console.error(err.message);
        if (err.kind === 'ObjectId') {
            return res.status(404).json({ msg: 'teacher not found' });
        }
        res.status(500).send('Server Error');
    }
});


// @route    GET /teachers
// @desc     Get all teachers
// @access   Public
router.get('/', async (req, res) => {
    try {
        // get all teacher as last record entered
        const teachers = await Teacher.find({}).sort({ _id: -1 });

        res.json(teachers);
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
});


// @route    GET /auth
// @desc     Test route
// @access   Public
router.get('/', auth, async (req, res) => {
    try {
        const teacher = await Teacher.findById(req.teacher.id).select('-password');
        res.json(teacher);
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
});



// @route    POST /teachers
// @desc     Register teacher
// @access   Public
router.post(
    '/',
    [
        // check('teacher_id', 'teacher_id is required').not().isEmpty(),
        check('name', 'Name is required').isAlpha().isLength({ min: 3 }),
        check('dob', 'dob is required').not().isEmpty(),
        check('fname', 'Father\'s name is required').isAlpha().isLength({ min: 3 }),
        check('mname', 'Mother\'s name is required').isAlpha().isLength({ min: 3 }),
        check('nationality', 'nationality is required').isAlpha().isLength({ min: 3 }),
        check('religion', 'religion is required').isAlpha().isLength({ min: 3 }),
        check('district', 'district is required').isAlpha().isLength({ min: 3 }),
        check('gender', 'gender is required').isAlpha().isLength({ min: 3 }),
        check('email', 'Please include a valid email').isEmail(),
        check('pincode', 'pincode is required').isNumeric().isLength(6),
        check('mobile', 'mobile is required').isNumeric().isLength(10),
        check('alternate_mobile', 'alternate_mobile is required').isNumeric().isLength(10),
        check(
            'password',
            'Please enter a password with 6 or more characters'
        ).isLength({ min: 6 })

    ],
    async (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }

        var { teacher_id,
            name,
            dob,
            fname,
            mname,
            nationality,
            religion,
            gender,
            address,
            district,
            email,
            pincode,
            mobile,
            alternate_mobile,
            password } = req.body;


        // to get last inserted teacher id
        try {
            var id = "tea1";

            var data = await Teacher.find({}).countDocuments();
            console.log('data:'+data);
            if(data){ 
                
            var stu = await Teacher.find({}, 'teacher_id').sort({ _id: -1 }).limit(1);

            var iiid = stu[0].teacher_id;
            console.log('iiid:'+iiid);
            var num = iiid.match(/\d+/g);
            // num[0] will be number
            var letr = iiid.match(/[a-zA-Z]+/g);
            // letr[0] will be string

            num = parseInt(num) + 1;
            id = letr + num.toString();
            teacher_id =  id;
            }


            if (teacher_id === "") {
                teacher_id = id;
            }


            console.log(teacher_id);

            let teacher = new Teacher({
                teacher_id,
                name,
                dob,
                fname,
                mname,
                nationality,
                religion,
                gender,
                address,
                district,
                email,
                pincode,
                mobile,
                alternate_mobile,
                password
            });



            // add salt for password hash
            const salt = await bcrypt.genSalt(10);
            // password with hash
            teacher.password = await bcrypt.hash(password, salt);

            await teacher.save();

            // _id used in token
            const payload = {
                teacher: {
                    id: teacher.id
                }
            };
            // for token
            jwt.sign(
                payload,
                config.get('jwtSecret'),
                { expiresIn: 360000 },
                (err, token) => {
                    if (err) throw err;
                    res.json({ token });
                }
            );
        } catch (err) {
            console.error(err.message);
            res.status(500).send('Server error');
        }
    }
);


// @route    put /teachers/update
// @desc     Create or update user profile
// @access   Private


router.post(
    '/update',
    [
        auth,
        [
            check('dateofhiring', 'dateofhiring is required').isNumeric().isLength({ min: 1 }),
            check('gender', 'gender is required').isAlpha().isLength({ min: 3 }),

        ]
    ],
    async (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }

        const {
            dateofhiring, gender
        } = req.body;
        const newval = {
            dateofhiring, gender
        };


        // if (skills) {
        //     profileFields.skills = skills.split(',').map(skill => skill.trim());
        // }


        try {
        console.log(req.teacher);

            var teacher = await Teacher.findOne({ _id: req.teacher.id });

            if (teacher) {
                // Update
                teacher = await Teacher.findOneAndUpdate(
                    { _id: req.teacher.id },
                    newval,
                    { new: true }
                );

                return res.json(teacher);
            }
            else {
                return res.status(400).json({ errors: errors.array() });
            }

            return res.json(teacher);

            // await teacher.save();
            // res.json(teacher);
        } catch (err) {
            console.error(err.message);
            res.status(500).send('Server Error');
        }
    }
);







//   --------------------------------------------------------------------------
//   --------------------------------------------------------------------------
//   --------------------------------------------------------------------------
//   --------------------------------------------------------------------------
//   --------------------------------------------------------------------------
//   --------------------------------------------------------------------------

// @route    POST /auth
// @desc     Authenticate teacher & get token
// @access   Public
router.post(
    '/auth',
    [
        check('teacher_id', 'Please include a valid teacher_id').not().isEmpty(),
        check('password', 'Password is required').exists()
    ],
    async (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }

        const { teacher_id, password } = req.body;

        try {
            let teacher = await Teacher.findOne({ teacher_id });

            if (!teacher) {
                return res
                    .status(400)
                    .json({ errors: [{ msg: 'Invalid Credentials' }] });
            }

            const isMatch = await bcrypt.compare(password, teacher.password);

            if (!isMatch) {
                return res
                    .status(400)
                    .json({ errors: [{ msg: 'Invalid Credentials' }] });
            }

            const payload = {
                teacher: {
                    id: teacher.id
                }
            };

            jwt.sign(
                payload,
                config.get('jwtSecret'),
                { expiresIn: 360000 },
                (err, token) => {
                    if (err) throw err;
                    res.json({ token });
                }
            );
        } catch (err) {
            console.error(err.message);
            res.status(500).send('Server error');
        }
    }
);


module.exports = router;
