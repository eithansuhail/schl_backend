const express = require('express');
const router = express.Router();
const { check, validationResult } = require('express-validator');
const Student = require('../models/Student');
const Attendance_student = require('../models/Attendance_stu');

// @route    DELETE /:attencance_id
// @desc     Delete a attencance
// @access   Public
router.delete('/:id', async (req, res) => {
    try {
        // Get sudent id for delete it.
        const attencance = await Attendance_student.findOne({ _id: req.params.id });

        if (!attencance) {
            return res.status(404).json({ msg: 'attencance not found' });
        }
        await attencance.remove();

        res.json({ msg: 'attencance removed' });
    } catch (err) {
        console.error(err.message);
        if (err.kind === 'ObjectId') {
            return res.status(404).json({ msg: 'attencance not found' });
        }
        res.status(500).send('Server Error');
    }
});


// @route    GET /attencance
// @desc     Get all attencance details
// @access   Public
router.get('/', async (req, res) => {
    try {
        // get all attencance as last record entered
        const attencances = await Attendance_student.find({}).sort({ _id: -1 });

        res.json(attencances);
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
});


// @route    GET /:attencance_id
// @desc     Test route
// @access   Public
router.put('/:attencance_id', async (req, res) => {
    try {
        const attencance = await Attendance_student.findById(req.params.attencance_id);
        res.json(attencance);
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
});


// @route    GET /student/:stu_id
// @desc     Test route
// @access   Public
router.put('/student/:stu_id', async (req, res) => {
    try {
        const attencance = await Attendance_student.find({ student_id: req.params.stu_id });
        if (!attencance)
            return res.status(400).send({ msg: 'invalid studend id' });
        res.json(attencance);
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
});


// @route    POST /attencance
// @desc     Register attencance
// @access   Public
router.post(
    '/',
    [
        check('attended', 'attended is required with only characters').isAlpha().isLength({ min: 3 }),
        check('date', 'date is required with only characters').isAlpha().isLength({ min: 3 }),
        check('student_id', 'student_id is required with only characters ans numbers').isAlphanumeric().isLength({ min: 3 }),
    ],
    async (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }

        var {
            attended,
            date,
            student_id
        } = req.body;


        try {

            let attencance = new Attendance_student({
                attended,
                date,
                student_id
    
            });

            const stu = await Student.findOne({ stu_id: student_id });
            if (stu) {

                await attencance.save();

                res.json(attencance);
            }
            else return res
                .status(400)
                .json({ errors: [{ msg: 'Invalid student ID' }] });


        }
        catch (err) {
            console.error(err.message);
            res.status(500).send('Server error');
        }
    }
);



module.exports = router;