const express = require('express');
const router = express.Router();
const { check, validationResult } = require('express-validator');

const Class = require('../models/Class');


// @route    POST /clasS
// @desc     Create or update user clasS
// @access   public
router.post(
    '/',
    [

        check('class_name', 'class_name is required')
            .not()
            .isEmpty(),
        check('description', 'description is required')
            .not()
            .isEmpty()
    ]
    ,
    async (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }

        const {

            class_name,
            description

        } = req.body;

        // Build clasS object
        const classFields = {
            class_name,
            description
        };

        try {
            let clasS = await Class.findOne({ class_name: req.body.class_name });

            if (clasS) {
                // Update
                clasS = await Class.findOneAndUpdate(
                    { class_name: req.body.class_name },
                    classFields ,
                    { new: true }
                );

                return res.json(clasS);
            }

            // Create
            clasS = new Class(classFields);

            await clasS.save();
            res.json(clasS);
        } catch (err) {
            console.error(err.message);
            res.status(500).send('Server Error');
        }
    }
);

// @route    GET /clasS
// @desc     Get all classes
// @access   Public
router.get('/', async (req, res) => {
    try {
        const classs = await Class.find({});
        res.json(classs);
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
});

// @route    GET /clasS/:name
// @desc     Get clasS by name
// @access   Public
router.get('/:name', async (req, res) => {
    try {
        const clasS = await Class.findOne({
            class_name: req.params.name
        });

        if (!clasS) return res.status(400).json({ msg: 'Class not found' });

        res.json(clasS);
    } catch (err) {
        console.error(err.message);
        if (err.kind == 'ObjectId') {
            return res.status(400).json({ msg: 'Class not found' });
        }
        res.status(500).send('Server Error');
    }
});

// @route    DELETE /:name
// @desc     Delete a class
// @access   Public


router.delete('/:name', async (req, res) => {
    try {
        const clasS = await Class.findOne({
            class_name: req.params.name
        });

        if (!clasS) return res.status(400).json({ msg: 'Class not found' });

        
        await clasS.remove();
        res.json({ msg: 'class removed' });
    } catch (err) {
        console.error(err.message);
        if (err.kind == 'ObjectId') {
            return res.status(400).json({ msg: 'Class not found' });
        }
        res.status(500).send('Server Error');
    }
});


module.exports = router;
