
const express = require('express');
const router = express.Router();
const auth = require('../middleware/authteacher');
const { check, validationResult } = require('express-validator');
const Notice = require('../models/Notice');

// @route    DELETE /:notice_id
// @desc     Delete a notice
// @access   Public
router.delete('/:id', async (req, res) => {
    try {
        // Get sudent id for delete it.
        const notice = await Notice.findOne({ _id: req.params.id });

        if (!notice) {
            return res.status(404).json({ msg: 'notice not found' });
        }
        await notice.remove();

        res.json({ msg: 'notice removed' });
    } catch (err) {
        console.error(err.message);
        if (err.kind === 'ObjectId') {
            return res.status(404).json({ msg: 'notice not found' });
        }
        res.status(500).send('Server Error');
    }
});


// @route    GET /notice
// @desc     Get all notice details
// @access   Public
router.get('/', async (req, res) => {
    try {
        // get all notice as last record entered
        const notices = await Notice.find({}).sort({ _id: -1 });

        res.json(notices);
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
});


// @route    GET /:notice_id
// @desc     Test route
// @access   Public
router.put('/:notice_id', async (req, res) => {
    try {
        const notice = await Notice.findById(req.params.notice_id);
        res.json(notice);
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
});




// @route    POST /notice
// @desc     Register notice
// @access   Public
router.post(
    '/',
    [
        check('subject', 'subject is required with only characters').isAlpha().isLength({ min: 3 }),
        check('issue_date', 'issue_date is required with only characters').isAlphanumeric().isLength({ min: 3 }),
        check('expiry_date', 'expiry_date is required with only characters').isLength({ min: 3 }),
        check('description', 'description is required with only characters').isLength({ min: 3 }),
        check('issued_by', 'issued_by is required with only characters').isAlpha().isLength({ min: 3 }),
        check('signature', 'signature is required with only characters').isAlpha().isLength({ min: 3 }),
        check('notice_id', 'notice_id is required with only characters ans numbers').isAlphanumeric().isLength({ min: 3 }),
    ],
    async (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }

        var {
            subject,
            description,
            issued_by,
            signature,
            notice_id,
            issue_date,
            expiry_date
        } = req.body;


        try {

            let notice = new Notice({
                subject,
                description,
                issued_by,
                signature,
                notice_id,
                issue_date,
                expiry_date
            });


                await notice.save();

                res.json(notice);
            
            

        }
        catch (err) {
            console.error(err.message);
            res.status(500).send('Server error');
        }
    }
);



module.exports = router;