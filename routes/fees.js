const express = require('express');
const router = express.Router();
const auth = require('../middleware/authteacher');
const { check, validationResult } = require('express-validator');
const Student = require('../models/Student');
const Fees = require('../models/Fees');

// @route    DELETE /:fees_id
// @desc     Delete a fees
// @access   Public
router.delete('/:id', async (req, res) => {
    try {
        // Get sudent id for delete it.
        const fees = await Fees.findOne({ _id: req.params.id });

        if (!fees) {
            return res.status(404).json({ msg: 'fees not found' });
        }
        await fees.remove();

        res.json({ msg: 'fees removed' });
    } catch (err) {
        console.error(err.message);
        if (err.kind === 'ObjectId') {
            return res.status(404).json({ msg: 'fees not found' });
        }
        res.status(500).send('Server Error');
    }
});


// @route    GET /fees
// @desc     Get all fees details
// @access   Public
router.get('/', async (req, res) => {
    try {
        // get all fees as last record entered
        const feess = await Fees.find({}).sort({ _id: -1 });

        res.json(feess);
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
});


// @route    GET /:fees_id
// @desc     Test route
// @access   Public
router.put('/:fees_id', async (req, res) => {
    try {
        const fees = await Fees.findById(req.params.fees_id);
        res.json(fees);
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
});


// @route    GET /student/:stu_id
// @desc     Test route
// @access   Public
router.put('/student/:stu_id', async (req, res) => {
    try {
        const fees = await Fees.find({ student_id: req.params.stu_id });
        if (!fees)
            return res.status(400).send({ msg: 'student has not submitted any fee yet' });
        res.json(fees);
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
});


// @route    POST /fees
// @desc     Register fees
// @access   Public
router.post(
    '/',
    [
        check('branchname', 'branchname is required with only characters').isAlpha().isLength({ min: 3 }),
        check('term', 'term is required with only characters').isAlpha().isLength({ min: 3 }),
        check('bankname', 'bankname is required with only characters').isAlpha().isLength({ min: 3 }),
        check('t_id', 't_id is required with only characters').isAlpha().isLength({ min: 3 }),
        check('student_id', 'student_id is required with only characters ans numbers').isAlphanumeric().isLength({ min: 3 }),
    ],
    async (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }

        var {
            branchname,
            term,
            bankname,
            t_id,
            student_id
        } = req.body;


        try {

            let fees = new Fees({
                branchname,
                term,
                bankname,
                t_id,
                student_id

            });

            const stu = await Student.findOne({ stu_id: student_id });
            if (stu) {

                await fees.save();

                res.json(fees);
            }
            else return res
                .status(400)
                .json({ errors: [{ msg: 'Invalid student ID' }] });


        }
        catch (err) {
            console.error(err.message);
            res.status(500).send('Server error');
        }
    }
);



module.exports = router;