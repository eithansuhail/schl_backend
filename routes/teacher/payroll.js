const express = require('express');
const router = express.Router();
const { check, validationResult } = require('express-validator');
const Teacher = require('../../models/Teacher');
const Payroll = require('../../models/Payroll');

// @route    DELETE /teacher/payroll/:id
// @desc     Delete a payroll
// @access   Public
router.delete('/:id', async (req, res) => {
    try {
        // Get sudent id for delete it.
        const payroll = await Payroll.findOne({ _id: req.params.id });

        if (!payroll) {
            return res.status(404).json({ msg: 'payroll not found' });
        }
        await payroll.remove();

        res.json({ msg: 'payroll removed' });
    } catch (err) {
        console.error(err.message);
        if (err.kind === 'ObjectId') {
            return res.status(404).json({ msg: 'payroll not found' });
        }
        res.status(500).send('Server Error');
    }
});


// @route    GET /teacher/payroll
// @desc     Get all payroll details
// @access   Public
router.get('/', async (req, res) => {
    try {
        // get all payroll as last record entered
        const payrolls = await Payroll.find({}).sort({ _id: -1 });

        res.json(payrolls);
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
});


// @route    GET /teacher/payroll/:payroll_id
// @desc     Test route
// @access   Public
router.get('/:payroll_id', async (req, res) => {
    try {
        const payroll = await Payroll.findById(req.params.payroll_id);
        res.json(payroll);
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
});


// @route    GET /teacher/payroll/:teacher_id
// @desc     Test route
// @access   Public
router.get('/:teacher_id', async (req, res) => {
    try {
        const payroll = await Payroll.find({ teacher_id: req.params.teacher_id });
        if (!payroll)
            return res.status(400).send({ msg: 'No data found.' });
        res.json(payroll);
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
});


// @route    POST /teacher/payroll
// @desc     Register payroll
// @access   Public
router.post(
    '/',
    [
        check('numofdayswork', 'numofdayswork is required with only characters').isNumeric().isLength({ min:1 }),
        check('bonus', 'bonus is required with only characters').isNumeric().isLength({ min: 3 }),
        check('overtimepay', 'overtimepay is required with only characters').isNumeric().isLength({ min: 3 }),
        check('cashadvanace', 'cashadvanace is required with only characters').isNumeric().isLength({ min: 3 }),
        check('latehours', 'latehours is required with only characters').isNumeric().isLength({ min: 1 }),
        check('absentdays', 'absentdays is required with only characters').isNumeric().isLength({ min: 1 }),
        check('totaldeduction', 'totaldeduction is required with only characters').isNumeric().isLength({ min: 3 }),
        check('netpay', 'netpay is required with only characters').isNumeric().isLength({ min: 3 }),
        check('month', 'month is required with only characters').isAlpha().isLength({ min: 3 }),
        check('teacher_id', 'teacher_id is required with only characters ans numbers').isAlphanumeric().isLength({ min: 3 }),
    ],
    async (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }

        var {
            teacher_id,
            numofdayswork,
            bonus,
            overtimepay,
            cashadvanace,
            latehours,
            absentdays,
            totaldeduction,
            netpay,
            month
        } = req.body;


        try {

            let payroll = new Payroll({
                teacher_id,
                numofdayswork,
                bonus,
                overtimepay,
                cashadvanace,
                latehours,
                absentdays,
                totaldeduction,
                netpay,
                month

            });

            const stu = await Teacher.findOne({ teacher_id: teacher_id });
            if (stu) {

                await payroll.save();

                res.json(payroll);
            }
            else return res
                .status(400)
                .json({ errors: [{ msg: 'Invalid teacher ID' }] });


        }
        catch (err) {
            console.error(err.message);
            res.status(500).send('Server error');
        }
    }
);



module.exports = router;