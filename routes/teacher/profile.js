const express = require('express');
const router = express.Router();
const auth = require('../../middleware/authteacher');

const { check, validationResult } = require('express-validator');

const Teacher = require('../../models/Teacher');
const Profile = require('../../models/TeacherProfile');

// @route    PUT /teacher/experience
// @desc     Add teacher experience
// @access   Private
router.put(
    '/experience',
    [
        auth,
        [
            check('title', 'Title is required')
                .not()
                .isEmpty(),
            check('school', 'school is required')
                .not()
                .isEmpty(),
            check('from', 'From date is required')
                .not()
                .isEmpty()
        ]
    ],
    async (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }

        const {
            title,
            school,
            location,
            from,
            to,
            current,
            description
        } = req.body;

        const profileFields = {};
        profileFields.teacher = req.teacher.id;
        profileFields.experience = {};
        profileFields.experience.school = school;
        profileFields.experience.location = location;
        profileFields.experience.from = from;
        profileFields.experience.to = to;
        profileFields.experience.title = title;
        profileFields.experience.current = current;
        profileFields.experience.description = description;


        try {
            let profile = await Profile.findOne({ teacher: req.teacher.id });
            console.log(req.teacher.id);
            if (profile) {

                profile = await Profile.findOneAndUpdate(
                    { teacher: req.teacher.id },
                    profileFields,
                    { new: true }
                );
                console.log(profile);
                return res.json(profile);
            }

            profile = new Profile(profileFields);

            await profile.save();
            res.json(profile);

        } catch (err) {
            console.error(err.message);
            res.status(500).send('Server Error');
        }
    }
);




// @route    PUT /teacher/education
// @desc     Add teacher education
// @access   Private
router.put(
    '/education',
    [
        auth,
        [
            check('degree', 'degree is required')
                .not()
                .isEmpty(),
            check('school', 'school is required')
                .not()
                .isEmpty(),
                check('fieldofstudy', 'fieldofstudy is required')
                .not()
                .isEmpty(),
            check('from', 'From date is required')
                .not()
                .isEmpty()
        ]
    ],
    async (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }

        const {
            degree,
            school,
            fieldofstudy,
            from,
            to,
            current,
            description
        } = req.body;

        const profileFields = {};
        profileFields.teacher = req.teacher.id;
        profileFields.education = {};
        profileFields.education.school = school;
        profileFields.education.fieldofstudy = fieldofstudy;
        profileFields.education.from = from;
        profileFields.education.to = to;
        profileFields.education.degree = degree;
        profileFields.education.current = current;
        profileFields.education.description = description;


        try {
            let profile = await Profile.findOne({ teacher: req.teacher.id });
            console.log(req.teacher.id);
            if (profile) {

                profile = await Profile.findOneAndUpdate(
                    { teacher: req.teacher.id },
                    profileFields,
                    { new: true }
                );
                console.log(profile);
                return res.json(profile);
            }

            profile = new Profile(profileFields);

            await profile.save();
            res.json(profile);

        } catch (err) {
            console.error(err.message);
            res.status(500).send('Server Error');
        }
    }
);



// @route    DELETE /teacher/profile/experience/:exp_id
// @desc     Delete experience from profile
// @access   Private
router.delete('/experience/:exp_id', auth, async (req, res) => {
  try {
    const profile = await Profile.findOne({ teacher: req.teacher.id });

    // Get remove index
    const removeIndex = profile.experience
      .map(item => item.id)
      .indexOf(req.params.exp_id);

    profile.experience.splice(removeIndex, 1);

    await profile.save();

    res.json(profile);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});




// @route    DELETE /teacher/profile/education/:edu_id
// @desc     Delete education from profile
// @access   Private
router.delete('/education/:edu_id', auth, async (req, res) => {
    try {
      const profile = await Profile.findOne({ teacher: req.teacher.id });
  
      // Get remove index
      const removeIndex = profile.education
        .map(item => item.id)
        .indexOf(req.params.edu_id);
  
      profile.education.splice(removeIndex, 1);
  
      await profile.save();
  
      res.json(profile);
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
  });
  
module.exports = router;