const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('config');
const auth = require('../middleware/authstudent');

const { check, validationResult } = require('express-validator');

const Student = require('../models/Student');

// @route    DELETE /:stu_id
// @desc     Delete a post
// @access   Public
router.delete('/:id', async (req, res) => {
    try {
        // Get sudent id for delete it.
        const student = await Student.findOne({ "stu_id": req.params.id });

        if (!student) {
            return res.status(404).json({ msg: 'student not found' });
        }
        await student.remove();

        res.json({ msg: 'student removed' });
    } catch (err) {
        console.error(err.message);
        if (err.kind === 'ObjectId') {
            return res.status(404).json({ msg: 'student not found' });
        }
        res.status(500).send('Server Error');
    }
});


// @route    GET /students
// @desc     Get all profiles
// @access   Public
router.get('/', async (req, res) => {
    try {
        // get all student as last record entered
        const students = await Student.find({}).sort({ _id: -1 });

        res.json(students);
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
});


// @route    POST /students
// @desc     Register student
// @access   Public
router.post(
    '/',
    [


        check('name', 'Name is required').isAlpha().isLength({ min: 3 }),
        check('dob', 'dob is required').not().isEmpty(),
        check('username', 'username is required').isAlpha().isLength({ min: 3 }),
        check('mobile', 'mobile is required').isNumeric().isLength(10),
        check(
            'password',
            'Please enter a password with 6 or more characters'
        ).isLength({ min: 6 })
    ],
    async (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }

        var {
            stu_id,
            name,
            username,
            dob,
            mobile,
            password
        } = req.body;


        // to get last inserted student id

        try {
            var id = "stu1";

            var data = await Student.find({}).countDocuments();
            console.log('data:' + data);
            if (data) {

                var stu = await Student.find({}, 'stu_id').sort({ _id: -1 }).limit(1);

                var iiid = stu[0].stu_id;
                console.log('iiid:' + iiid);
                var num = iiid.match(/\d+/g);
                // num[0] will be number
                var letr = iiid.match(/[a-zA-Z]+/g);
                // letr[0] will be string

                num = parseInt(num) + 1;
                id = letr + num.toString();

            }


            if (stu_id === "") {
                stu_id = id;
            }


            console.log(stu_id);

            let student = new Student({
                stu_id,
                name,
                username,
                dob,
                username,
                mobile,
                password
            });



            // add salt for password hash
            const salt = await bcrypt.genSalt(10);
            // password with hash
            student.password = await bcrypt.hash(password, salt);

            await student.save();

            // _id used in token
            const payload = {
                student: {
                    id: student.id
                }
            };
            // for token
            jwt.sign(
                payload,
                config.get('jwtSecret'),
                { expiresIn: 360000 },
                (err, token) => {
                    if (err) throw err;
                    res.json({ token });
                }
            );
        } catch (err) {
            console.error(err.message);
            res.status(500).send('Server error');
        }
    }
);


// @route    POST /update
// @desc     update Student
// @access   Private
router.post(
    '/update',
    [
        auth,
        [
            check('yearofenroll', 'yearofenroll is required').isNumeric().isLength({ min: 1 }),
            check('gender', 'gender is required').not().isEmpty(),

        ]
    ],
    async (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }

        const {
            class_name, yearofenroll, gender, section
        } = req.body;
        const newval = {
            class_name, yearofenroll, gender, section
        };


        // if (skills) {
        //     profileFields.skills = skills.split(',').map(skill => skill.trim());
        // }



        try {
            console.log(req.student);
            var student = await Student.findOne({ _id: req.student.id });

            if (student) {
                // Update
                student = await Student.findOneAndUpdate(
                    { _id: req.student.id },
                    newval,
                    { new: true }
                );

                return res.json(student);
            }
            else {
                return res.status(400).json({ errors: errors.array() });
            }

            return res.json(student);

            // await student.save();
            // res.json(student);
        } catch (err) {
            console.error(err.message);
            res.status(500).send('Server Error');
        }
    }
);





// @route    GET /auth
// @desc     Test route
// @access   Public
router.get('/', auth, async (req, res) => {
    try {
        const student = await Student.findById(req.student.id).select('-password');
        res.json(student);
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
});



//   --------------------------------------------------------------------------
//   --------------------------------------------------------------------------
//   --------------------------------------------------------------------------
//   --------------------------------------------------------------------------
//   --------------------------------------------------------------------------
//   --------------------------------------------------------------------------

// @route    POST /auth
// @desc     Authenticate student & get token
// @access   Public
router.post(
    '/auth',
    [
        check('stu_id', 'Please include a valid stu_id').not().isEmpty(),
        check('password', 'Password is required').exists()
    ],
    async (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }

        const { stu_id, password } = req.body;

        try {
            let student = await Student.findOne({ stu_id });

            if (!student) {
                return res
                    .status(400)
                    .json({ errors: [{ msg: 'Invalid Credentials' }] });
            }

            const isMatch = await bcrypt.compare(password, student.password);

            if (!isMatch) {
                return res
                    .status(400)
                    .json({ errors: [{ msg: 'Invalid Credentials' }] });
            }

            const payload = {
                student: {
                    id: student.id
                }
            };

            jwt.sign(
                payload,
                config.get('jwtSecret'),
                { expiresIn: 360000 },
                (err, token) => {
                    if (err) throw err;
                    res.json({ token });
                }
            );
        } catch (err) {
            console.error(err.message);
            res.status(500).send('Server error');
        }
    }
);


module.exports = router;
