const mongoose = require('mongoose');

const TeacherSchema = new mongoose.Schema({
    teacher_id: {
        type: String,
        unique: true,
        required: true
    },
    name: {
        type: String,
        required: true
    },

    dob: {
        type: String,
        required: true
    },
    fname: {
        type: String,
        required: true
    },
    mname: {
        type: String,
        required: true
    },
    nationality: {
        type: String,
        required: true
    },
    religion: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: true
    },
    district: {
        type: String,
        required: true
    },
    pincode: {
        type: Number,
        required: true,
        length: 6
    },
    mobile: {
        type: Number,
        required: true,
        length: 10

    },
    alternate_mobile: {
        type: Number,
        required: true,
        length: 10

    },

    email: {
        type: String,
        required: true
    },


    password: {
        type: String,
        required: true
    },

    gender: {
        type: String
    },
    dateofhiring: {
        type: String
    },
    yearsofserving: {
        type: String
    },
    speciality: {
        type: String
    },salary: {
        type: Number
    },
},
    {
        timestamps: true,
    }
);

TeacherSchema.index({ name: 1, dob: 1, fname: 1 }, { unique: true });
module.exports = Teacher = mongoose.model('teacher', TeacherSchema);