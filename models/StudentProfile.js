const mongoose = require('mongoose');

const ProfileSchema = mongoose.Schema({
    student: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'student'
    },
    name: {
        type: String,
        required: true
    },

    fname: {
        type: String,
        required: true
    },
    mname: {
        type: String,
        required: true
    },
    feducation: {
        type: String,
        required: true
    },
    meducation: {
        type: String,
        required: true
    },
    foccupation: {
        type: String,
        required: true
    },
    moccupation: {
        type: String,
        required: true
    },
    nationality: {
        type: String,
        required: true
    },
    religion: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: true
    },
    district: {
        type: String,
        required: true
    },
    pincode: {
        type: Number,
        required: true,
        length: 6
    },
    alternate_mobile: {
        type: Number,
        required: true,
        length: 10

    },
    email: {
        type: String,
        required: true
    },
    guardian_details: {
        type: String,
        required: true
    },
    class_name: {
        type: String,
        required: true
    },
    gender: {
        type: String
    },
    yearofenroll: {
        type: String
    },



});


module.exports = Profile = mongoose.model('studentProfile', ProfileSchema);
