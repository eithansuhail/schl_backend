const mongoose = require('mongoose');

const FeesSchema = new mongoose.Schema({
    student_id: {
        type: String,
        required: true,
        ref:'student'
    },
    term: {
        type: String,
        required: true
    },
    bankname: {
        type: String,
        required: true
    },
    branchname: {
        type: String,
        required: true
    },
    t_id: {
        type: String,
        required: true
    },
    


},
    {
        timestamps: true,
    }
);

// FeesSchema.index({ stu_id: 1, dob: 1, fname: 1 }, { unique: true });
module.exports = Fees = mongoose.model('fees', FeesSchema);