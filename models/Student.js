const mongoose = require('mongoose');

const StudentSchema = new mongoose.Schema({
    stu_id: {
        type: String,
        unique: true,
        required: true
    },
    stu_UId: {
        type: String,
        unique: true,
        
    },
    username: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    dob: {
        type: String,
        required: true
    },
    mobile: {
        type: Number,
        required: true,
        length: 10

    },
    password: {
        type: String,
        required: true
    },

}
    ,
    {
        timestamps: true,
    }
);

StudentSchema.index({ name: 1, dob: 1, fname: 1 }, { unique: true });

module.exports = Student = mongoose.model('student', StudentSchema);