const mongoose = require('mongoose');

const NoticeSchema = new mongoose.Schema({
    notice_id: {
        type: String,
        required: true,
        ref:'student'
    },
    subject: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    issued_by: {
        type: String,
        required: true
    },
    signature: {
        type: String,
        required: true
    },
    issue_date: {
        type: String,
        required: true
    },
    expiry_date: {
        type: String,
        required: true
    },

},
    {
        timestamps: true,
    }
);

// NoticeSchema.index({ stu_id: 1, dob: 1, fname: 1 }, { unique: true });
module.exports = Notice = mongoose.model('notice', NoticeSchema);