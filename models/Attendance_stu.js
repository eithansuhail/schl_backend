const mongoose = require('mongoose');

const Attendance_studentSchema = new mongoose.Schema({
    student_id: {
        type: String,
        unique: true,
        required: true,
        ref: 'student'
    },
    attended: {
        type: Boolean,
        default: false,
        required: true
    },
    date: {
        type: String,
        required: true
    }


},
    {
        timestamps: true,
    }
);

// Attendance_studentSchema.index({ stu_id: 1, dob: 1, fname: 1 }, { unique: true });
module.exports = Attendance_student = mongoose.model('student_schema', Attendance_studentSchema);