const mongoose = require('mongoose');

const ClassSchema = new mongoose.Schema({
   
    class_name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },


},
    {
        timestamps: true,
    }
);

module.exports = Class = mongoose.model('clasS', ClassSchema);