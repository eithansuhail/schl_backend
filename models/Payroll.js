const mongoose = require('mongoose');

const PayrollSchema = new mongoose.Schema({
    teacher_id: {
        type: String,
        required: true,
        ref: 'teacher'
    },
    numofdayswork: {
        type: Number,
        required: true
    },
    bonus: {
        type: Number,
        required: true
    },
    overtimepay: {
        type: Number,
        required: true
    },
    cashadvanace: {
        type: Number,
        required: true
    },
    latehours: {
        type: Number,
        required: true
    },
    absentdays: {
        type: Number,
        required: true
    },
    totaldeduction: {
        type: Number,
        required: true
    }, 
    netpay: {
        type: Number,
        required: true
    },month:{
        type:String,
        required:true
    }


},
    {
        timestamps: true,
    }
);

// PayrollSchema.index({ stu_id: 1, dob: 1, fname: 1 }, { unique: true });
module.exports = Payroll = mongoose.model('payroll', PayrollSchema);