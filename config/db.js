const mongoose = require('mongoose');
const config = require('config');
const db = config.get('mongoURI');

// Connect to Mongo
mongoose.connect(db, { useNewUrlParser: true, useCreateIndex: true,useFindAndModify:false }
);
const connection = mongoose.connection;
connection.once('open', () => {
    console.log("MongoDB database connection established successfully");
});

    
const connectDB = async () => {
    try {
        await mongoose.connect(db, 
            { useNewUrlParser: true, useCreateIndex: true,useFindAndModify:false }
            );
        console.log('MongoDB Connected...')

    } catch (err) {
        console.log(err.message);

        // Exit process with failure
        process.exit(1);

    }
}

module.exports = connectDB;