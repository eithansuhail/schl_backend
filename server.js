const express = require('express');
const connectDB = require('./config/db');
const path = require('path');

const app = express();

// Connect Database
connectDB();

// Init Middleware
app.use(express.json({ extended: false }));

// Define Routes
app.use('/students', require('./routes/students'));
app.use('/teachers', require('./routes/teachers'));
app.use('/fees', require('./routes/fees'));
app.use('/notice', require('./routes/notice'));
app.use('/teacher/profile', require('./routes/teacher/profile'));
app.use('/teacher/sendmail', require('./routes/teacher/sendmail'));
app.use('/teacher/payroll', require('./routes/teacher/payroll'));
app.use('/classes',require('./routes/classes'));



const PORT = process.env.PORT || 8000;

app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
